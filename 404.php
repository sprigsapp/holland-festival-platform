<?php get_header(); ?>

<div class="container">
    <div class="row align-items-center flex-column py-5">
        <h1>404 Not found!</h1>
        <p>It looks like nothing was found at this location.</p>
    </div>
</div>
<?php get_footer(); ?>