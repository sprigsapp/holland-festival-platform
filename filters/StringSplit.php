<?php


class StringSplit
{
    private $string;
    public function __construct($string)
    {
        $this->string = strip_white_spaces($string);
    }

    public function getStrings(){
        return preg_split('/<br[^>]*>/i', $this->string);
    }

    public function stringLen()
    {
        return count($this->getStrings());
    }

    public function fullString()
    {
        return implode('',$this->getStrings());
    }

    public function slug()
    {
        return strtolower(str_replace(' ', '_', $this->fullString()));
    }



}