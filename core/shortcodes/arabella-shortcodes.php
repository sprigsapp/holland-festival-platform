<?php
/*Accordion*/
function arabella_accordion_shortcode($atts, $content){
    extract(shortcode_atts( array('title' => ''), $atts));
    $return_statement = "
    <h4 class='element-header'>{$title}</h4>
    <div>
        <p>{$content}</p>
    </div>
";
    return $return_statement;
}
add_shortcode('accordion', 'arabella_accordion_shortcode');

function arabella_accordiongroup_shortcode($atts,$content){
    $content = do_shortcode($content);
    $return_statement = "
    <div class='shortcodes accordion'>
        {$content}
    </div>
";
    return $return_statement;
}
add_shortcode('accordiongroup', 'arabella_accordiongroup_shortcode');
/*Accordion*/

/*Toggles*/
function arabella_toggle_shortcode($atts, $content){
    extract(shortcode_atts( array('title' => ''), $atts));
    $return_statement = "
    <h4 class='element-header'>{$title}</h4>
    <div>
        <p>{$content}</p>
    </div>
";
    return $return_statement;
}
add_shortcode('toggle', 'arabella_toggle_shortcode');

function arabella_togglegroup_shortcode($atts,$content){
    $content = do_shortcode($content);
    $return_statement = "
    <div class='shortcodes toggles'>
        {$content}
    </div>
";
    return $return_statement;
}
add_shortcode('togglegroup', 'arabella_togglegroup_shortcode');
/*Toggles*/

/*Tabs*/
$UM_GLOBAL_TABS = array();
global $UM_GLOBAL_TABS;
function arabella_tab_shortcode($atts, $content){
    extract(shortcode_atts( array('title' => ''), $atts));
    global $UM_GLOBAL_TABS;
    array_push($UM_GLOBAL_TABS,array($title,$content));
    return "";
}
add_shortcode('tab', 'arabella_tab_shortcode');

function arabella_tabgroup_shortcode($atts, $content){
    $content = do_shortcode($content);
    global $UM_GLOBAL_TABS;

    $position = $atts['position'];
    $return_statemenet = '
    <div class="filter-tabs clearfix shortcodes '.$position.'">
    <ul class="clearfix">';
    $i = 0;
    foreach($UM_GLOBAL_TABS as $tab){
        $i++;
        $class = "";
        if($i == 1){
            $class = "active";
        }
        $return_statemenet .= '<li><a class="'.$class.'" id="tab'.$i.'" href="javascript:void(0)" >'.$tab[0].'</a></li>';
    }
    $return_statemenet .= '</ul>';
    $j = 0;
    foreach($UM_GLOBAL_TABS as $tab){
        $j++;
        $return_statemenet .= '
        <div class="tabs">
            <div class="tab" id="tab'.$j.'C">
            <p>'.$tab[1].'</p>
            </div>
        </div>';
    }
    $return_statemenet .= '</ul>
     </div>';

    $UM_GLOBAL_TABS = array();
    return $return_statemenet;


}

add_shortcode('tabgroup', 'arabella_tabgroup_shortcode');
/*Tabs*/

/*Notification*/
function arabella_notification_shortcode($atts,$content){
    extract(shortcode_atts( array('title' => '','type'=>''), $atts));
    return "<div class='{$type} alert alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' area-hidden='true'>&times;</button>
                        <strong>{$title} </strong>{$content}
                    </div>";
}
add_shortcode("notification","arabella_notification_shortcode");
/*Notification*/

/*Dropcaps*/
function arabella_dropcap1_shortcode($atts,$content){
    return "<span class='dropcap_01'>{$content}</span>";
}

add_shortcode("dropcap1","arabella_dropcap1_shortcode");

function arabella_dropcap2_shortcode($atts,$content){
    return "<span class='dropcap_02'>{$content}</span>";
}

add_shortcode("dropcap2","arabella_dropcap2_shortcode");
/*Dropcaps*/

/*Highlights*/
function arabella_highlight_shortcode($atts,$content){
    return "<div class='half-box'>{$content}</div>";
}

add_shortcode("highlight","arabella_highlight_shortcode");
/*Highlights*/

/*Boxed Content*/
function arabella_boxed_shortcode($atts,$content){
    return "<div class='background_box'>
                <p>{$content}</p>
            </div>";
}

add_shortcode("boxed","arabella_boxed_shortcode");

function arabella_boxed2_shortcode($atts,$content){
    return "<div class='simple_box'>
                <p>{$content}</p>
            </div>";
}

add_shortcode("boxed2","arabella_boxed2_shortcode");
/*Boxed Content*/

/*Buttons*/
function arabella_button_shortcode($atts,$content){
    extract(shortcode_atts( array('type'=>'','url'=>''), $atts));

    return "<a href='{$url}' data-lightbox='lightbox' class=''>{$content}</a>";
}
add_shortcode("lightbox","arabella_button_shortcode");
/*Buttons*/

/*Video*/
function arabella_video_shortcode($atts,$content){
    extract(shortcode_atts( array('poster'=>'','videomp4'=>'','videovp8'=>'','video_ogg'=>''), $atts));
    $sources = "";
    if($videomp4){
        $sources .= "<source type='video/mp4' src='{$videomp4}' />";
    }
    if($videovp8){
        $sources .= "<source type='video/webm' src='{$videovp8}' />";
    }
    if($video_ogg){
        $sources .= "<source type='video/ogg' src='{$video_ogg}' />";
    }

    return "<video controls='controls' preload='none' poster='{$poster}' width='480' height='270'>
                {$sources}
            </video>";
}
add_shortcode("video","arabella_video_shortcode");
/*Video*/

//*Layout*/
function arabella_full_width_shortcode($atts,$content){
    $content = do_shortcode($content);
    return "<div class='col-md-12'><p>{$content}</p></div>";
}
add_shortcode("full_width","arabella_full_width_shortcode");

function arabella_half_width_shortcode($atts,$content){
    $content = do_shortcode($content);
    return "<div class='col-md-6'><p>{$content}</p></div>";
}
add_shortcode("half_width","arabella_half_width_shortcode");

function arabella_one_third_shortcode($atts,$content){
    $content = do_shortcode($content);
    return "<div class='col-md-4'><p>{$content}</p></div>";
}
add_shortcode("one_third","arabella_one_third_shortcode");

function arabella_one_fourth_shortcode($atts,$content){
    $content = do_shortcode($content);
    return "<div class='col-md-3'><p>{$content}</p></div>";
}
add_shortcode("one_fourth","arabella_one_fourth_shortcode");

function arabella_one_sixth_shortcode($atts,$content){
    $content = do_shortcode($content);
    return "<div class='col-md-2'><p>{$content}</p></div>";
}
add_shortcode("one_sixth","arabella_one_sixth_shortcode");

function arabella_layout_group_shortcode($atts,$content){
    $content = do_shortcode($content);
    return "<div class='halfRow'>{$content}</div>";
}
add_shortcode("layout_group","arabella_layout_group_shortcode");

function arabella_layout_shortcode($atts,$content){
    $content = do_shortcode($content);
    return "<div class='row'>
                {$content}
            </div>";
}
add_shortcode("layout_btn","arabella_layout_shortcode");
/*Layout*/

function logo_shortcode($atts,$content){
    //$content = do_shortcode($content);
    return '<span class="letter-k">K</span>';
}
add_shortcode("logo", "logo_shortcode");
/*Layout*/

/*Generics*/
add_action('after_setup_theme', 'add_buttons');
function add_buttons() {
    if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
        return;
    }
    if ( get_user_option('rich_editing') == 'true' ) {
        add_filter( 'mce_external_plugins', 'add_plugin' );
        add_filter( 'mce_buttons_3', 'register_button' );
    }
}


function register_button( $buttons ) {
    array_push( $buttons, "separator", "logo" );
    //array_push( $buttons, "separator", "toggle_btn" );
    //array_push( $buttons, "separator", "tab_btn" );
    //array_push( $buttons, "separator", "alert_btn" );
   	array_push( $buttons, "separator", "highlight_btn" );
    array_push( $buttons, "separator", "button_btn" );
    //array_push( $buttons, "separator", "video_btn" );
    //array_push( $buttons, "separator", "layout_btn" );

    return $buttons;
}

function add_plugin( $plugin_array ) {
    $plugin_array['logo'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    //$plugin_array['toggle_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    //$plugin_array['tab_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    //$plugin_array['alert_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    //$plugin_array['dropcap1_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    //$plugin_array['dropcap2_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    $plugin_array['highlight_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    //$plugin_array['boxed_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    //$plugin_array['boxed2_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    $plugin_array['button_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    //$plugin_array['video_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    // $plugin_array['layout_btn'] = get_template_directory_uri() .'/vendor/shortcodes/'  . 'tiny_mce_buttons.js';
    return $plugin_array;
}
/*Generics*/

require_once "dialog-forms.php";