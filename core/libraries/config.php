<?php

$path = 'config';

$files = dirToArray($path);

function setNameKey($files)
{
    $results = [];

    foreach ($files as $file) {
        array_push($results, ['name' => str_replace('.php', '', $file), 'file' => $file]);
    }

    return $results;
}

function config($filename){
    if(file_exists(get_template_directory() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . $filename . '.php')){
        if (!defined($filename)) {
            DEFINE($filename, include(get_template_directory() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . $filename . '.php'));
            return constant($filename);
        }
    }
}

