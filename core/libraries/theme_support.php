<?php
register_nav_menus( array(
    'primary' => __( 'Main menu', 'web_lang' ),
) );

add_theme_support( 'post-thumbnails' );
add_image_size('full-hd', 1280, 0, false);

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function fix_svg_thumb_display() {
  return '
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail {
      width: 100% !important;
      height: auto !important;
    }
  ';
}
add_action('admin_head', 'fix_svg_thumb_display');

//hide wp and wpml versions
show_admin_bar( false );
remove_action('wp_head', 'wp_generator');
if ( ! empty ( $GLOBALS['sitepress'] ) ) {
    add_action( 'wp_head', function()
    {
        remove_action(
            current_filter(),
            array ( $GLOBALS['sitepress'], 'meta_generator_tag' )
        );
    },
    0
    );
}

/*  CPT filters*/
// include_once ('cpt_filters.php');
// new Tax_CTP_Filter(array('post' => array('priority')));
// new Tax_CTP_Filter(array('post' => array('magazine')));
