<?php

// 1. customize ACF path
add_filter('acf/settings/path', 'my_acf_settings_path');
function my_acf_settings_path( $path ) {
    // update path
    $path = get_stylesheet_directory() . '/vendor/acf/';
    // return
    return $path;
}

// 2. customize ACF dir
add_filter('acf/settings/dir', 'my_acf_settings_dir');
function my_acf_settings_dir( $dir ) {
    // update path
    $dir = get_stylesheet_directory_uri() . '/vendor/acf/';
    // return
    return $dir;
}

// 3. Hide ACF field group menu item
add_filter('acf/settings/show_admin', '__return_true');

// 4. Include ACF
include_once( get_stylesheet_directory() . '/vendor/acf/acf.php' );

// setup optionspage
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

	if( function_exists('acf_set_options_page_title') )
	{
	    acf_set_options_page_title( __('Theme Options') );
	}

	// acf_add_options_sub_page('Header');
	acf_add_options_sub_page('Default settings');
}

/* Custom selects */
include_once "acf_custom_select.php";

// include_once(get_stylesheet_directory() . '/vendor/acf-addons/gforms-acf/acf-gravity_forms.php');
// if(function_exists('acf_add_options_page')) {
// 	acf_add_options_page();
// }

add_filter('acf/settings/google_api_key', function () {
    return 'AIzaSyBsV4IXB4MmqfH5uE4SlNkl-flqcd8WSuU';
});