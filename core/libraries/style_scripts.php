<?php

function web_styles()
{
	
	$cssFiles = array(
	    'dist/css/app.css' => array('name' => 'app-css','internal' => true),
		//'css/bootstrap.min.css' => array('name' => 'bootstrap', 'internal' => true),
		//'https://fonts.googleapis.com/css?family=Raleway:300,400,500,700' => array('name' => 'google-fonts', 'internal' => false),
	);
	foreach($cssFiles as $filePath => $type)
	{
		if(!$type['internal'])
			wp_enqueue_style($type['name'], $filePath, array(), THEME_VERSION);
		else
			wp_enqueue_style($type['name'], assets_path($filePath), array(), THEME_VERSION);
	}
}
add_action('wp_enqueue_scripts', 'web_styles');

function web_scripts()
{
	wp_enqueue_script('main', assets_path('dist/js/app.js'), array(), THEME_VERSION, true);	
	//wp_enqueue_script('web-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', array(), THEME_VERSION, true);
	//wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBsV4IXB4MmqfH5uE4SlNkl-flqcd8WSuU', array(), THEME_VERSION, true);
	//wp_enqueue_script('main', get_template_directory_uri().'/assets/js/main.js', array(), THEME_VERSION, true);
}
add_action('wp_enqueue_scripts', 'web_scripts');


//DASHBOARD Script & Styles
function admin_style() {
//    wp_enqueue_style('admin-styles', get_template_directory_uri().'/public/assets/css/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');

function admin_script() {
//    wp_enqueue_script('admin-js', get_template_directory_uri().'/public/assets/js/admin.js');
}
add_action('admin_enqueue_scripts', 'admin_script');
?>