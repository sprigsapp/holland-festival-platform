<?php


function apiCall($url) {
	$request = wp_remote_get($url);
	
	$object = null;
	if(isset($request['body']))
		$object = json_decode($request['body']);
	
	return $object;	
}
