<?php 

//twitter feeds
function get_twitter_feeds($username, $count = 10)
{
    $consumer_key = 'EBTzAZsWA4DxM7RHE68InV29Q';
    $consumer_secret = 'Pu6tVrWQ2ElEjfL68kRiEKys2ZveovJhbis267mN7aqlUEZ6jt';
    $user_token = '1045540874-jWfZAPSGjFj6yxCdAdhIfP3z8apEUHJ3uFhFDdb';
    $user_secret = 'h1DrsKETfUi8TeHNqznRxhfwBn0qMxoK6d17QaNgtDsuz';

    if ( !class_exists('TwitterOAuth') ){
        require get_template_directory().'/vendor/twitter-oAuth/twitteroauth.php';
    }

    $twitter = new TwitterOAuth($consumer_key, $consumer_secret, $user_token, $user_secret);
    $feed = $twitter->get('https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name='.$username.'&count='.$count);

    if($feed){
        $tweets = [];
        foreach ($feed as $key => $tweet) {
            $tweets[] = addTweetEntityLinks($tweet);
        }

        return $tweets;

    } else {
        return false;
    }
}

function addTweetEntityLinks( $tweet )
{
    // actual tweet as a string
    $tweetText = $tweet->text;

    // create an array to hold urls
    $tweetEntites = array();

    // add each url to the array
    foreach( $tweet->entities->urls as $url ) {
        $tweetEntites[] = array (
                'type'    => 'url',
                'curText' => substr( $tweetText, $url->indices[0], ( $url->indices[1] - $url->indices[0] ) ),
                'newText' => "<a href='".$url->expanded_url."' target='_blank'>".$url->display_url."</a>"
            );
    }  // end foreach

    // add each user mention to the array
    foreach ( $tweet->entities->user_mentions as $mention ) {
        $string = substr( $tweetText, $mention->indices[0], ( $mention->indices[1] - $mention->indices[0] ) );
        $tweetEntites[] = array (
                'type'    => 'mention',
                'curText' => substr( $tweetText, $mention->indices[0], ( $mention->indices[1] - $mention->indices[0] ) ),
                'newText' => "<a href='http://twitter.com/".$mention->screen_name."' target='_blank'>".$string."</a>"
            );
    }  // end foreach

    // add each hashtag to the array
    foreach ( $tweet->entities->hashtags as $tag ) {
        $string = substr( $tweetText, $tag->indices[0], ( $tag->indices[1] - $tag->indices[0] ) );
        $tweetEntites[] = array (
                'type'    => 'hashtag',
                'curText' => substr( $tweetText, $tag->indices[0], ( $tag->indices[1] - $tag->indices[0] ) ),
                'newText' => "<a href='http://twitter.com/search?q=%23".$tag->text."&src=hash' target='_blank'>".$string."</a>"
            );
    }  // end foreach

    // replace the old text with the new text for each entity
    foreach ( $tweetEntites as $entity ) {
        $feeds = array(
            'url' => 'https://twitter.com/StoryMakerOrg/status/'.$tweet->id,
            'tweet' => str_replace( $entity['curText'], $entity['newText'], $tweetText )
        );
    } // end foreach

    return $feeds;

}