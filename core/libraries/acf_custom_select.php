<?php

function load_field_category( $field ) {

    $terms = get_terms(array('category'), array(
        'orderby' => 'name',
        'order' => 'ASC',
        'hide_empty' => false,
    ));

   	$field['choices'] = array();

    if( is_array($terms) ) {

        foreach( $terms as $term ) {

            $field['choices'][ $term->slug ] = $term->name;

        }

    }
    return $field;

}

add_filter('acf/load_field/name=category', 'load_field_category');