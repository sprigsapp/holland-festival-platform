<?php
function dd($obj)
{
    die(var_dump($obj));
}

function dpr($obj, $pre = true)
{
    if ($pre)
        echo "<pre>";
    die(print_r($obj));
}

function isAjax()
{
    $headers = apache_request_headers();
    return (isset($headers['X-Requested-With']) && $headers['X-Requested-With'] == 'XMLHttpRequest');
}

function sprigs_load_template($name, $params = array())
{
    set_query_var('data', $params);

    return get_template_part('parts/' . $name);
}

function sprigs_get_categories($taxonomy, $order_by = 'DESC', $hide_empty = true, $exclude = '', $limit)
{
    $args = array(
        'type' => 'post',
        'orderby' => 'name',
        'order' => $order_by,
        'hide_empty' => $hide_empty,
        'taxonomy' => $taxonomy,
        'exclude' => $exclude,
        'number' => $limit
    );
    $data = get_categories($args);
    return $data;
}

function string_from_categories($categories, $delimiter = ' ')
{
    $result = array();

    foreach ($categories as $category) {
        $result[] = $category['name'];
    }
    return implode($delimiter, $result);
}

function ul_from_categories($categories, $key, $seperator = '', $classes = ' ')
{
    $data = array();
    foreach ($categories as $category) {
        $tag = $category[$key];
        $url = get_term_link($category['id'], $category['taxonomy']);
        $data[] = '<li><a href="' . $url . '">' . $tag . '</a></li>';
    }
    return '<ul class="' . $classes . '">' . implode($seperator, $data) . '</ul>';
}

function sprigs_excerpt_length($length)
{
    return 20;
}

add_filter('excerpt_length', 'sprigs_excerpt_length', 999);
function sprigs_excerpt_more($more)
{
    return '...';
}

add_filter('excerpt_more', 'sprigs_excerpt_more');
function sprigs_excerpt($string, $word_limit = 10)
{
    $string = strip_tags($string);
    $words = explode(" ", $string);
    $return = implode(" ", array_splice($words, 0, $word_limit));
    if (strlen($string) > strlen($return)) {
        return $return .= "...";
    } else {
        return esc_attr($string);
    }
}

function sprigs_pagination($paged, $wp_query, $pages = '', $range = 2)
{
    $showitems = ($range * 2) + 1;
    if (is_null($paged)) {
        global $paged;
        if (empty($paged)) $paged = 1;
    }
    if (is_null($wp_query)) {
        global $wp_query;
    }
    if ($pages == '') {
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }
    if (1 != $pages) {
        echo "<ul class='list-inline pagination-ef'>";
        //if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a class='page-numbers' href='".get_pagenum_link(1)."'><span class='arrows'>&laquo;</span> First</a></li>";
        if ($paged > 1) echo "<li><a aria-label='Previous' href='" . get_pagenum_link($paged - 1) . "'><i class='fa fa-chevron-left'></i></a></li>";
        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                echo ($paged == $i) ? "<li><span class='current'>" . $i . "</span></li>" : "<li><a class='page-numbers' href='" . get_pagenum_link($i) . "' class='inactive' >" . $i . "</a></li>";
            }
        }
        if ($paged < $pages) echo "<li><a aria-label='Next' href='" . get_pagenum_link($paged + 1) . "'><i class='fa fa-chevron-right'></i></a></li>";
        //if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a class='page-numbers' href='".get_pagenum_link($pages)."'>Last <span class='arrows'>&raquo;</span></a></li>";
        echo "</ul>\n";
    }
}

add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);
function special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3);
function remove_thumbnail_dimensions($html, $post_id, $post_image_id)
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

function get_first_sentence($string)
{
    $sentence = preg_split('/[?!;:\.]/', $string, 2, PREG_SPLIT_DELIM_CAPTURE);
    $sentence['0'] = str_replace('<p>', '', $sentence['0']);

    return $sentence['0'] . '...';
}

function list_languages($class = "")
{
    $languages = icl_get_languages('skip_missing=0');
    $html = '<div class="' . $class . '">';

    foreach ($languages as $language) {
        $active_lang = $language['active'] ? 'active' : '';
        $html .= '<a href="' . $language['url'] . '" class="' . $active_lang . '">' . $language['code'] . '</a>' . "\n";
    }
    $html .= '</div>';
    return $html;
}

function get_media_url($postID, $size = 'large')
{
    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($postID), $size);

    if (!$thumb) return '';
    return $thumb['0'];
}

function get_media_url_by_id($mediaID, $size = 'large')
{
    $thumb = wp_get_attachment_image_src($mediaID, $size);

    if (!$thumb) return '';
    return $thumb['0'];
}

function get_media_attr($mediaID, $attr = 'post_excerpt')
{
    $media = get_post($mediaID);
    if (isset($media->{$attr})) return $media->{$attr};
    return '';
}

function sprigs_get_segment($segment = 0)
{
    $_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $segments = explode('/', $_SERVER['REQUEST_URI_PATH']);

    return esc_attr($segments[$segment]);
}

function sprigs_current_url()
{
    return "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
}

function site_title()
{
    return wp_title('-', false, 'right') . get_bloginfo('name');
}

function assets_path($path)
{
    return esc_url(get_template_directory_uri() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . $path);
}

function get_page_by_template($templateName, $firstPage = true)
{
    $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => "$templateName.php",
    ));

    if ($firstPage) {
        return $pages[0];
    } else {
        return $pages;
    }
}

function is_current_template($templateName)
{
    return get_page_template_slug() == $templateName . '.php';
}

function strip_white_spaces($string, $allowHTML = false)
{
    $result = str_replace(array("\n", "\r"), '', $string);
    return $allowHTML ? nl2br($result) : $result;
}