<?php 

function getYouTubeIdFromURL($url)
{
    $url_string = parse_url($url, PHP_URL_QUERY);
    parse_str($url_string, $args);
    return isset($args['v']) ? $args['v'] : false;
} 

function getVimeoIdFromURL($url)
{
    return substr(parse_url($url, PHP_URL_PATH), 1);
} 


function get_video_thumbnail($videoID, $video_type='vimeo', $size = null){
	$result = null;
	
	if(isset($video_type)){
		switch ($video_type) {
			case 'vimeo':
				$result = get_vimeo_thumbnail($videoID, $size);
				break;
			
			case 'youtube':
				$result = get_youtube_thumbnail($videoID, $size);
				break;	

			default:
				
				break;
		}
	}

	return $result;
}

function get_vimeo_thumbnail($videoID, $size){

	$video_result = json_decode(file_get_contents("https://vimeo.com/api/v2/video/$videoID.json"));
	$video = $video_result[0];

	if(! is_null($size)){
		if(isset($video->$size)){
			return $video->$size;
		}
		else{
			return null;
		}
	}
	else{
		return $video->thumbnail_medium;
	}
}

function get_youtube_thumbnail($videoID, $size){
	if(!is_null($size)){
		return "http://img.youtube.com/vi/$videoID/$size.jpg";
	}
	else{
		return "http://img.youtube.com/vi/$videoID/hqdefault.jpg";
	}
}