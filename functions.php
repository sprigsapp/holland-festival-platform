<?php

define('THEME_VERSION', date('Ymd'));

/* Register Dir Support */
include_once "core/libraries/dir_files.php";

/* Load style and scripts */
include_once "core/libraries/style_scripts.php";

/* Register Theme Support */
include_once "core/libraries/helpers.php";

/* Register Config Support */
include_once "core/libraries/config.php";

/* Register Theme Support */
include_once "core/libraries/theme_support.php";

/* Ajax actions */
include_once "core/libraries/ajax.php";

/* Register Popular Posts */
//include_once "core/libraries/popular_posts.php";

/* Register Shortcodes */
//include_once('core/shortcodes/arabella-shortcodes.php');

/* Disable emails */
//include_once('core/disable-emails/disable-emails.php');

/* Register Twitter Helper */
//include_once "core/libraries/twitter-helper.php";

/* Video Thumbnail Helpers */
//include_once "core/libraries/video_thumbnail_helper.php";


if (!is_user_logged_in()) {
    add_filter('show_admin_bar', '__return_false');
} else {
    add_filter('show_admin_bar', '__return_true');
}

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

add_filter('use_block_editor_for_post', '__return_false', 10);