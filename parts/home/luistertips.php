<?php if (have_rows('lustertips')) : ?>
    <?php while (have_rows('lustertips')) :
        the_row(); ?>
        <?php sprigs_load_template('home/parts/title-wrapper') ?>

        <section class="section-videos-list-horizontal bg--dark" id="<?php the_sub_field('menu_anchor'); ?>">
            <div class="container-fluid">

                <?php $audio_list = get_sub_field('audio_list'); ?>
                <?php if ($audio_list) : ?>
                    <div class="row scroll-horizontal-mobile">
                        <?php foreach ($audio_list as $post) : ?>
                            <?php setup_postdata($post); ?>
                            <div class="col-12 col-sm-3">
                                <div class="video__wrapper video__wrapper--square">
                                    <div class="video__details">
                                        <h2 class="video__title"><?php echo $post->post_title ?></h2>
                                        <div class="video__data">
                                            <?php $terms = get_the_terms($post->ID, 'maker'); ?>
                                            <?php if ($terms) : ?>
                                                <?php for ($i = 0; $i < count($terms); $i++) : ?>
                                                    <?php echo $terms[$i]->name . ' ' ?>
                                                <?php endfor; ?>
                                            <?php endif; ?>
                                            <br> <?php echo get_field('audio_duration') ?>
                                        </div>
                                    </div>
                                    <a href="<?php echo get_field('link') ? get_field('link') : 'javascript:void(0);' ?>" <?php echo get_field('link') ? ' target="_blank" style="cursor: default;" ' : '' ?> class="video__iframe" style="background-image: url('<?php echo get_the_post_thumbnail_url() ?>');">  
                                    <?php if (get_field('show_play_button')): ?>    
                                            <i class="icon-play"></i>
                                        <?php endif; ?>
                                    </a>
                                    <div class="video__details">
                                        <div class="video__description collapsed">
                                            <p>
                                                <?php echo get_field('intro_text') ?>
                                            </p>
                                            <div class="collapsable">
                                                <?php echo get_field('collapsable') ?>
                                            </div>
                                            <?php sprigs_load_template('home/parts/collapse-toggler') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>