<?php if (get_field('collapsable')): ?>
    <a class="collapse-toggler" href="javascript:void(0);">
        <span class="read-more"><?php echo __('lees meer', 'HF') ?> ↓</span>
        <span class="read-less"><?php echo __('lees minder', 'HF') ?> ↑</span>
    </a>
<?php endif; ?>
