<?php $stringSplit = new StringSplit(get_sub_field('title')); ?>
<div class="title-wrapper">
    <svg class="<?php echo $stringSplit->stringLen() == 1 ? "single-row" : '' ?>">
        <defs>
            <mask id="mask-<?php echo $stringSplit->slug()?>" x="0" y="0" width="100%" height="100%">
                <rect class="alpha" x="0" y="0" width="100%" height="100%"/>
                <text class="d-none d-xl-block title" x="50%">
                    <tspan dy="270px"><?php echo $stringSplit->fullString()?></tspan>
                </text>
                <text class="d-none d-md-block d-xl-none title" x="50%">
                    <tspan dy="220px"><?php echo $stringSplit->fullString() ?></tspan>
                </text>
                <text class="d-block d-md-none title" x="50%">
                    <tspan dy="105px"><?php echo $stringSplit->getStrings()[0]?></tspan>
                </text>
                <?php if ($stringSplit->stringLen() > 1): ?>
                    <text class="d-block d-md-none title" x="50%">
                        <tspan dy="169px"><?php echo $stringSplit->getStrings()[1]?></tspan>
                    </text>
                <?php endif; ?>
            </mask>
        </defs>
        <rect class="base" style="mask: url(#mask-<?php echo $stringSplit->slug()?>);" x="0" y="0" width="100%" height="100%"/>
    </svg>
</div>