<?php if (have_rows('recipe_list')) : ?>
    <?php while (have_rows('recipe_list')) : the_row(); ?>
        <section class="section-recipes bg--dark" id="<?php the_sub_field('menu_anchor'); ?>">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <h1 class="section__title section__title--small"><?php echo strip_white_spaces(get_sub_field('title')); ?></h1>
                    </div>
                </div>


                <div id="recipes-scroll-anchor"></div>

                <?php $recipes = get_sub_field('recipes'); ?>
                <?php if ($recipes) : ?>
                    <?php foreach ($recipes as $post) :  ?>
                        <?php setup_postdata($post); ?>
                        <div class="single-recipe" id="recipe-<?php echo get_the_ID(); ?>">
                            <div class="row mb-5">
                                <div class="col-12 col-sm-6 mb-5">
                                    <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
                                </div>
                                <div class="col-12 col-sm-6">
                                    <h2><?php the_title(); ?></h2>
                                    <div class="large">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3 mb-sm-5">
                                <div class="col-12 col-sm-3 mb-3">
                                    <h3><?php the_sub_field('ingredients_label'); ?></h3>
                                    <?php echo get_field('ingredients'); ?>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <h3><?php the_sub_field('instructions_label'); ?></h3>
                                    <?php echo get_field('preperations'); ?>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>


                <div class="row scroll-horizontal-mobile">
                    <?php if ($recipes) : ?>
                        <?php foreach ($recipes as $post) :  ?>
                            <?php setup_postdata($post); ?>
                            <div class="col-12 col-sm-4">
                                <a href="javascript:void(0);" data-id="recipe-<?php echo get_the_ID(); ?>" class="recipe__grid-item recipe-toggler" style="background-image:url('<?php echo get_the_post_thumbnail_url() ?>')">
                                    <h2 class="recipe__title">
                                        <?php the_title(); ?> →
                                    </h2>
                                </a>
                            </div>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
                <hr>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>