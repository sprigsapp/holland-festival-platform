<?php if (have_rows('video_list_vertical')) : ?>
    <?php while (have_rows('video_list_vertical')) : the_row(); ?>
        <?php sprigs_load_template('home/parts/title-wrapper') ?>

        <section class="section-videos-list-verical bg--dark" id="<?php the_sub_field('menu_anchor'); ?>">
            <div class="container-fluid">
                <?php $video_list = get_sub_field('video_list'); ?>
                <?php if ($video_list) : ?>
                    <?php foreach ($video_list as $post) : ?>
                        <?php setup_postdata($post); ?>
                        <div class="row flex-sm-row-reverse video__wrapper">
                            <div class="col-12 col-sm-6">
                                <div class="video__iframe">
                                    <iframe width="560" height="315"
                                            src="https://www.youtube.com/embed/<?php echo get_field('youtube_id'); ?>"
                                            frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;"
                                            allowfullscreen></iframe>
                                </div>
                            </div>

                            <div class="col-12 col-sm-6">
                                <div class="video__details">
                                    <h2 class="video__title">
                                        <?php echo $post->post_title ?></h2>
                                    <div class="video__data">
                                        <?php $terms = get_the_terms($post->ID, 'maker'); ?>
                                        <?php if ($terms) : ?>
                                            <?php echo $terms[0]->name ?> <br>
                                        <?php endif; ?>
                                        <?php $viewOnLinks = get_field('view_on'); ?>
                                        <?php if ($viewOnLinks) : ?>
                                            Te zien op
                                            <?php for ($i = 0; $i < count($viewOnLinks); $i++) : ?>
                                                <?php $link = $viewOnLinks[$i]['link']; ?>
                                                <?php if ($link) : ?>
                                                    <a href="<?php echo $link['url']; ?>"
                                                       target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?>
                                                    </a>
                                                    <?php if (count($viewOnLinks) > $i + 1) : ?>,<?php endif; ?>
                                                <?php endif; ?>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="video__description collapsed">
                                        <p>
                                            <?php echo get_field('intro_text') ?>
                                        </p>
                                        <div class="collapsable">
                                            <?php echo get_field('collapsable') ?>
                                        </div>
                                        <?php sprigs_load_template('home/parts/collapse-toggler') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
                <hr>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>