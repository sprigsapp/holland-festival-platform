<?php if (have_rows('puzzle')) : ?>
    <?php while (have_rows('puzzle')) : the_row(); ?>
    <?php sprigs_load_template('home/parts/title-wrapper') ?>
        <section class="section-puzzle bg--dark" id="<?php the_sub_field('menu_anchor'); ?>">
            <div class="container-fluid">
                <div class="row pb-5">
                    <div class="col-12 col-sm-6 mb-5">
                        <img src="<?php echo assets_path('img/puzzle.png'); ?>" alt="">
                    </div>

                    <div class="col-12 col-sm-6 d-flex">
                        <div class="d-flex flex-column justify-content-center align-items-center w-100">
                            <h2 class="text-center mb-5"><?php echo strip_white_spaces(get_sub_field('description')); ?></h2>
                            <a href="<?php the_sub_field('link'); ?>" target="_blank" class="button">Ga naar de puzzelpagina</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="bg--dark"><div class="container-fluid"><hr></div></section>

        

    <?php endwhile; ?>
<?php endif; ?>