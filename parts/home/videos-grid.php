<?php if (have_rows('video_grid')) : ?>
    <?php while (have_rows('video_grid')) : the_row(); ?>
        <?php sprigs_load_template('home/parts/title-wrapper') ?>
        <section class="section-videos-grid bg--dark" id="<?php the_sub_field('menu_anchor'); ?>">
            <div class="container-fluid">


                <div class="row  scroll-horizontal-mobile">
                    <?php $video_list = get_sub_field('video_list'); ?>
                    <?php if ($video_list) : ?>
                        <?php foreach ($video_list as $post) : ?>
                            <?php setup_postdata($post); ?>

                            <div class="col-12 col-sm-6 col-lg-4 video__wrapper">
                                <div class="video__iframe">
                                    <iframe width="560" height="315"
                                            src="https://www.youtube.com/embed/<?php echo get_field('youtube_id'); ?>"
                                            frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;"
                                            allowfullscreen></iframe>
                                </div>
                                <h2 class="video__title">
                                    <?php the_title(); ?>
                                </h2>
                                <?php $terms = get_the_terms($post->ID, 'maker'); ?>
                                <?php if ($terms): ?>
                                    <div class="video__data">
                                        <?php for ($i = 0; $i < count($terms); $i++) : ?>
                                            <?php echo $terms[$i]->name ?><?php echo count($terms) > $i + 1 ? ',' : '' ?>
                                        <?php endfor; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>


                <div class="d-flex flex-column align-items-center mb-5">
                    <?php $button = get_sub_field('button'); ?>
                    <?php if ($button) : ?>
                        <a class="button mb-3" href="<?php echo esc_url($button['url']); ?>"
                           target="<?php echo esc_attr($button['target']); ?>"><?php echo esc_html($button['title']); ?></a>
                    <?php endif; ?>
                    <?php $link = get_sub_field('link'); ?>
                    <?php if ($link) : ?>
                        <a class="link" href="<?php echo esc_url($link['url']); ?>"
                           target="<?php echo esc_attr($link['target']); ?>"><?php echo esc_html($link['title']); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>