<?php if (have_rows('snacktips')) : ?>
    <?php while (have_rows('snacktips')) : the_row(); ?>
        <?php sprigs_load_template('home/parts/title-wrapper') ?>

        <section class="section-snacks bg--dark" id="<?php the_sub_field('menu_anchor'); ?>">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-3 mb-5">
                        <?php $image = get_sub_field('image'); ?>
                        <?php if ($image) : ?>
                            <img src="<?php echo esc_url($image['url']); ?>"
                                 alt="<?php echo esc_attr($image['alt']); ?>"/>
                        <?php endif; ?>
                    </div>
                    <div class="col-12 col-lg-6 col-sm-9">
                        <div class="large">
                            <p>
                                <?php the_sub_field('description'); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>