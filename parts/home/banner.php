<?php $banner = get_field('banner'); ?>
<?php if (count($banner['image_list'])) : ?>


    <section class="section-banner" id="<?php the_sub_field('menu_anchor'); ?>">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <div class="countdown__wrapper" data-images-count="<?php echo count($banner['image_list']); ?>">
                        <div class="images-reel">
                            <div class="images-wrapper">
                                <?php for ($i = 0; $i < count($banner['image_list']); $i++): ?>
                                    <?php $image = $banner['image_list'][$i] ?>
                                    <div class="image" style="background-image: url(<?php echo $image['image']['sizes']['large']; ?>);"></div>
                                <?php endfor ?>
                            </div>
                        </div>
                        <div class="countdown"></div>
                        <div class="images-reel images-reel--down">
                            <div class="images-wrapper" style="animation-duration: <?php echo count($banner['image_list']).'s'?>">
                                    <?php for ($i = 0; $i < count($banner['image_list']); $i++): ?>
                                        <?php $index = 0; ?>
                                        <?php if ($i == 0): ?>
                                            <?php $index = count($banner['image_list']) - 1; ?>
                                        <?php else: ?>
                                            <?php $index = $i - 1; ?>
                                        <?php endif; ?>
                                        <?php $image = $banner['image_list'][$index] ?>
                                        <div class="image" style="background-image: url(<?php echo $image['image']['sizes']['large']; ?>);"></div>
                                    <?php endfor ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="text__wrapper">
                        <h1><?php echo strip_white_spaces($banner['description']); ?>
                            <br>
                            <span>&nbsp;</span>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>