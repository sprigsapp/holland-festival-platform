<?php if (have_rows('hero_banner')) : ?>
    <?php while (have_rows('hero_banner')) : the_row(); ?>
        <section class="section-banner-hero pb-0 blend" id="<?php the_sub_field('menu_anchor'); ?>">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <h1 class="section__title"><?php echo strip_white_spaces(get_sub_field('title'))  ; ?></h1>
                    </div>
                </div>
            </div>
        </section>
        <section class="section-banner-hero pt-0">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-8">
                        <div class="section-banner-hero__description">
                            <?php the_sub_field('description'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>
