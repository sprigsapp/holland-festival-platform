<?php if (have_rows('books_listing')) : ?>
    <?php while (have_rows('books_listing')) : the_row(); ?>
        <?php sprigs_load_template('home/parts/title-wrapper') ?>

        <section class="section-books-listing bg--dark" id="<?php the_sub_field('menu_anchor'); ?>">
            <div class="container-fluid">
                <?php $books = get_sub_field('books'); ?>
                <?php if ($books) : ?>
                    <?php foreach ($books as $post) : ?>
                        <?php setup_postdata($post); ?>
                        <div class="row d-none d-sm-flex justify-content-center book__wrapper">
                            <div class="col-12 col-sm-6">
                                <div class="book__cover">
                                    <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
                                </div>
                            </div>

                            <div class="col-12 col-sm-6">
                                <div class="book__details">
                                    <h2 class="book__title"><?php the_title(); ?></h2>
                                    <?php $terms = get_the_terms($post->ID, 'writer'); ?>
                                    <?php if ($terms) : ?>
                                        <div class="book__data">
                                            <?php echo $terms[0]->name ?>
                                        </div>
                                    <?php endif; ?>
                                    <div class="book__description collapsed">
                                        <p>
                                            <?php echo get_field('intro_text') ?>
                                        </p>
                                        <div class="collapsable">
                                            <?php echo get_field('collapsable') ?>
                                        </div>
                                        <?php sprigs_load_template('home/parts/collapse-toggler') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>

                <!-- mobile -->
                <?php if ($books) : ?>
                    <div class="row d-sm-none scroll-horizontal-mobile mb-5">
                        <?php foreach ($books as $post) : ?>
                            <?php setup_postdata($post); ?>

                            <div class="col-12">
                                <div class="book__cover mb-4">
                                    <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="">
                                </div>

                                <div class="book__details">
                                    <h2 class="book__title"><?php the_title(); ?></h2>
                                    <?php $terms = get_the_terms($post->ID, 'writer'); ?>
                                    <?php if ($terms) : ?>
                                        <div class="book__data">
                                            <?php echo $terms[0]->name ?>
                                        </div>
                                    <?php endif; ?>
                                    <div class="book__description collapsed">
                                        <p>
                                            <?php echo get_field('intro_text') ?>
                                        </p>
                                        <div class="collapsable">
                                            <?php echo get_field('collapsable') ?>
                                        </div>
                                        <?php sprigs_load_template('home/parts/collapse-toggler') ?>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach; ?>
                    </div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
                <hr>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>