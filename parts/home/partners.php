<?php if (have_rows('partners')) : ?>
    <?php while (have_rows('partners')) : the_row(); ?>
        <section class="section-partners bg--dark" id="<?php the_sub_field('menu_anchor'); ?>">
            <div class="container-fluid pb-4">
            <div class="row">
                    <div class="col-12">
                        <h1 class="section__title section__title--small"><?php echo strip_white_spaces(get_sub_field('title')); ?></h1>
                    </div>
                </div>
                <div class="partners-wrapper">
                    <?php if (have_rows('groups')) : ?>
                        <?php while (have_rows('groups')) : the_row(); ?>
                            <?php if (get_row_layout() == 'group') : ?>
                                <div class="partners-group text-center">
                                    <div class="partner-label text-center mb-4"><?php the_sub_field('label') ?></div>
                                    <div class="partners-group-images">
                                        <?php if (have_rows('partner')) : ?>
                                            <?php while (have_rows('partner')) : the_row(); ?>

                                                <div class="partner-item">
                                                    <?php $image = get_sub_field('image'); ?>
                                                    <?php if ($image) { ?>
                                                        <img src="<?php echo $image['url']; ?>"
                                                             alt="<?php echo $image['alt']; ?>"/>
                                                    <?php } ?>
                                                    <?php $link = get_sub_field('link'); ?>
                                                    <?php if ($link) { ?>
                                                        <a href="<?php echo $link['url']; ?>"
                                                           target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
                                                    <?php } ?>
                                                </div>

                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>