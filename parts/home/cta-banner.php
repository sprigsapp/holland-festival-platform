<?php if (have_rows('cta_banner')) : ?>
    <?php while (have_rows('cta_banner')) : the_row(); ?>
        <section class="section-banner-cta pt-5" id="<?php the_sub_field('menu_anchor'); ?>">
            <div class="container">
                <h1><?php echo strip_white_spaces(get_sub_field('title')); ?></h1>
                <p><?php the_sub_field('description'); ?></p>
                <?php $button = get_sub_field('button'); ?>
                <?php if ($button) : ?>
                    <a class="button mb-5" href="<?php echo esc_url($button['url']); ?>"
                       target="<?php echo esc_attr($button['target']); ?>"><?php echo esc_html($button['title']); ?></a>
                <?php endif; ?>
            </div>
        </section>
    <?php endwhile; ?>
<?php endif; ?>
