</main>
<footer class="bg--dark">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8">
                <div class="section__title">
                    <h2>HOLLAND<br>FESTIVAL</h2>
                </div>
            </div>
            <div class="col-sm-4">
                <p>
                    <?php the_field('address', 'option'); ?>
                </p>
                <p>
                    <?php echo __('Telefoon', 'HF'); ?> <a
                            href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a>
                    <br>
                    <?php echo __('e-mail', 'HF'); ?> <a
                            href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                </p>

                <p>
                    <?php echo __('Blijf op de hoogte via:', 'HF'); ?>
                </p>
                <div class="social-media">

                    <?php if (get_field('social_media', 'option')) : ?>
                        <?php foreach (get_field('social_media', 'option') as $key => $media) : ?>
                            <a href="<?php echo $media; ?>" class="social-media--icon"><i
                                        class="icon-<?php echo $key; ?>"></i></a>
                        <?php endforeach ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>

</html>