const body = document.getElementsByTagName('body')[0];

document.addEventListener('DOMContentLoaded', () => {
    objectFitImages(document.querySelectorAll('.book__cover img'));

    menuFunctionality();

    document.addEventListener('scroll', () => {
        handleMenuTogglerColor();
    });

    setTimeout(() => {
        body.classList.remove('loading');
    }, 200);
    bannerCountdown();

    backgroundCanvas();
    // stickySections();

    accordions();
    recipes();
});

let lastMenuSection = '';
let lightMenuClasses = ['bg--dark', 'title-wrapper'];

const handleMenuTogglerColor = () => {
    let closestSection = document.elementFromPoint(1, 1);

    if (closestSection.tagName == 'DIV' && (closestSection.closest('section').classList.contains('bg--dark'))) {
            body.classList.add('menu--light');
    } else if (closestSection.tagName == 'rect') {
        body.classList.add('menu--light');
    } else {
        body.classList.remove('menu--light');
    }
};

const stickySections = () => {
    let heroBanner = document.querySelectorAll('.section-banner-hero');
    if (!heroBanner) return;
    let heroHeight = heroBanner[0].scrollHeight + heroBanner[1].scrollHeight;
    let windowHeight = document.documentElement.clientHeight;

    document.addEventListener('resize', () => {
        heroHeight = heroBanner[0].scrollHeight + heroBanner[1].scrollHeight;
        windowHeight = document.documentElement.clientHeight;
    });
    document.addEventListener('scroll', () => {
        if (window.scrollY + windowHeight > heroHeight) {
            if (window.scrollY > heroHeight) {
                heroBanner[1].querySelector(
                    '.container-fluid'
                ).style.transform = `translateY(0px)`;
                return;
            }
            heroBanner[1].querySelector(
                '.container-fluid'
            ).style.transform = `translateY(${
                window.scrollY + windowHeight - heroHeight
            }px)`;
        } else {
            heroBanner[1].querySelector(
                '.container-fluid'
            ).style.transform = `translateY(0px)`;
        }
    });
};
const bannerCountdown = () => {
    if (!document.querySelector('.countdown')) return;
    countdown(
        new Date(2021, 0, 1),
        (ts) =>
            (document.querySelector('.countdown').innerHTML = `${
                ts.hours
            }:${prependZero(ts.minutes)}:${prependZero(ts.seconds)}`),
        countdown.HOURS | countdown.MINUTES | countdown.SECONDS
    );

    // Start images reel
    let countdownWrapper = document.querySelector('.countdown__wrapper');
    countdownWrapper.classList.add('playing');
    // 1400 is the transition speed;
    let repeatInterval = countdownWrapper.getAttribute('data-images-count') * 1400;
    
    setInterval(() => {
        countdownWrapper.classList.remove('playing');
        setTimeout(() => {
            countdownWrapper.classList.add('playing');
        }, 100);
    }, repeatInterval);
    
    
};

const prependZero = (nr) => (nr > 9 ? nr : `0${nr}`);

const backgroundCanvas = () => {
    const c = document.getElementById('iridescence');
    const canvas = c.getContext('2d');

    const col = (x, y, r, g, b) => {
        canvas.fillStyle = `rgb(${r},${g},${b})`;
        canvas.fillRect(x, y, 1, 1);
    };
    const R = (x, y, t) => {
        return Math.floor(192 + 64 * Math.cos((x * x - y * y) / 100 + t));
    };

    const G = (x, y, t) => {
        return Math.floor(
            192 +
                64 *
                    Math.sin(
                        (x * x * Math.cos(t / 4) + y * y * Math.sin(t / 3)) /
                            150
                    )
        );
    };

    const B = (x, y, t) => {
        return Math.floor(
            192 +
                64 *
                    Math.sin(
                        5 * Math.sin(t / 9) +
                            ((x - 100) * (x - 100) + (y - 100) * (y - 100)) /
                                800
                    )
        );
    };

    let t = 0;

    const run = function () {
        for (x = 0; x <= 35; x++) {
            for (y = 0; y <= 35; y++) {
                col(x, y, R(x, y, t), G(x, y, t), B(x, y, t));
            }
        }
        t = t + 0.1;
    };
    run();
    window.addEventListener('scroll', () => {
        run();
    });
};
const menuFunctionality = () => {
    let menuToggle = document.getElementById('js-menu-toggle');

    if (!menuToggle) return;

    menuToggle.addEventListener('click', () => {
        body.classList.toggle('menu-open');
    });

    let anchorMenuItems = document.getElementsByClassName(
        'menu-item-type-custom'
    );
    if (anchorMenuItems) {
        [...anchorMenuItems].forEach((anchorMenuItem) => {
            anchorMenuItem.querySelector('a').addEventListener('click', (e) => {
                e.preventDefault();
                _scrollTo(e.target.getAttribute('href'), -100);
                body.classList.toggle('menu-open');
            });
        });
    }
};
const accordions = () => {
    let togglers = document.querySelectorAll('.collapse-toggler');

    [...togglers].forEach((toggler) => {
        toggler.addEventListener('click', function () {
            let parent = this.parentElement;
            let collapsable = parent.querySelector('.collapsable');

            parent.classList.toggle('collapsed');
            if (collapsable.style.maxHeight) collapsable.style.maxHeight = null;
            else
                collapsable.style.maxHeight =
                    collapsable.scrollHeight + 54 + 'px';
        });
    });
};
const recipes = () => {
    let togglers = document.querySelectorAll('.recipe-toggler');
    let recipeItems = document.querySelectorAll('.single-recipe');

    [...togglers].forEach((toggler, index) => {
        let recipeId = toggler.getAttribute('data-id');

        toggler.addEventListener('click', function () {
            let recipeItem = document.getElementById(recipeId);
            [...recipeItems].forEach((item) => (item.style.maxHeight = null));
            setTimeout(() => {
                recipeItem.style.maxHeight = `${recipeItem.scrollHeight}px`;
                _scrollTo('#recipes-scroll-anchor', -100);
            }, 500);
        });
    });
};
const _scrollTo = (selector, yOffset = 0) => {
    const el = document.querySelector(selector);
    if (!el) return;
    const y = el.getBoundingClientRect().top + window.pageYOffset + yOffset;

    window.scrollTo({ top: y, behavior: 'smooth' });
};
