<?php 
/*
 *  Template name: Homepage
 */
?>

<?php get_header(); ?>
<?php sprigs_load_template('home/hero-banner') ?>
<?php sprigs_load_template('home/videos-list-vertical') ?>
<?php sprigs_load_template('home/luistertips') ?>
<?php sprigs_load_template('home/banner') ?>
<?php sprigs_load_template('home/books-listing') ?>
<?php sprigs_load_template('home/puzzle') ?>
<?php sprigs_load_template('home/snacktips') ?>
<?php sprigs_load_template('home/recipes-list') ?>
<?php sprigs_load_template('home/videos-grid') ?>
<?php sprigs_load_template('home/cta-banner') ?>
<?php sprigs_load_template('home/partners') ?>

<?php get_footer(); ?>