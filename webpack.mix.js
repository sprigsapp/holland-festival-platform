const mix = require('laravel-mix');

mix.setPublicPath(path.resolve('./'));

var HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
mix.webpackConfig({
    plugins: [new HardSourceWebpackPlugin()],
});

mix.copy('resources/assets/js/vendor', 'public/assets/dist/js/vendor');

mix.options({
    processCssUrls: false,
});

mix.sass('resources/assets/sass/app.scss', 'public/assets/dist/css');
mix.sourceMaps(false, 'source-map');

// mix.styles(
//     ['public/assets/dist/css/app.css'],
//     'public/assets/dist/css/app.css',
//     './'
// );

mix.version('public/assets/dist/css/app.css');

mix.js('resources/assets/js/app.js', 'public/assets/dist/js');

mix.version('public/assets/dist/js/app.js');
