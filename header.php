<!DOCTYPE html>
<html class="no-js">

<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-0FKQ9CLT4N"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-0FKQ9CLT4N');
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo site_title() ?></title>

    <meta name="msapplication-tap-highlight" content="yes" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <?php if (get_field('fav_icon', 'options')) : ?>
        <!-- Favicon -->
        <?php $favIcon = get_field('fav_icon', 'options'); ?>
        <link rel="icon" type="image/ico" href="<?php echo esc_attr($favIcon['url']) ?>">
        <link rel="apple-touch-icon" href="<?php echo esc_attr($favIcon['url']) ?>">
    <?php endif; ?>

    <!-- Mobile -->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="<?php bloginfo('name'); ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?>">
    <meta name="theme-color" content="">

    <!-- Facebook OG -->
    <?php sprigs_load_template('facebook_meta') ?>

    <script>
        var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
        var template_directory = "<?php echo get_template_directory_uri() ?>";
    </script>
    <?php wp_head(); ?>

</head>

<body class="loading">
    <header class="header">

        <nav>
            <?php wp_nav_menu(['menu' => 'primary', 'container' => '', 'menu_id' => '', 'menu_class' => 'menu']); ?>
        </nav>

        <div class="navigation-toggle" id="js-menu-toggle">
            <span></span>
            <span></span>
            <span></span>
        </div>

    </header>
    <main>
        <div class="background-canvas">
            <canvas id="iridescence" width="32" height="32"></canvas>
        </div>